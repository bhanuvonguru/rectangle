package com.vapasi.thoughtworks.test;

import com.vapasi.thoughtworks.Rectangle;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest {

    Rectangle rect;

    @Test
    void shouldCalculateAreaForRectangle() {
        rect = new Rectangle(10, 20);
        assertEquals(200, rect.calculateArea());
    }

    @Test
    void shouldCalculatePerimeterForRectangle() {
        rect = new Rectangle(20, 30);
        assertEquals(100, rect.calculatePerimeter());
    }

    @Test
    void shouldCalculateAreaForSquare() {
        Rectangle square = Rectangle.createSquare(10);
        assertEquals(100, square.calculateArea());
    }

    @Test
    void shouldCalculatePerimeterForSquare() {
        Rectangle square = Rectangle.createSquare(10);
        assertEquals(40, square.calculatePerimeter());
    }
}
