package com.vapasi.thoughtworks;

public class Rectangle {
    private double breadth;
    private double length;

    public Rectangle(int length, int breadth) {
        this.length = length;
        this.breadth = breadth;
    }


    public static Rectangle createSquare(int side){
        Rectangle rectangle = new Rectangle(side,side);
        return rectangle;
    }

    public double calculateArea() {
        return length * breadth;

    }

    public double calculatePerimeter() {
        return 2 * (length + breadth);
    }
}
